$(document).ready(function() {
	$(window).bind("load resize scroll", function() {
        if ($(document).scrollTop() > 174) {
            $('.box-menu-header').addClass('header-fixed');
        } else {
            $('.box-menu-header').removeClass('header-fixed');
        }
    });

    // Menu Mobile
    function closeMenu() {
        $('.y-mobile-menu').removeClass('show');
        $('.overlay-menu').removeClass('active');
    }
    $(".has-submenu > .btn-toggle-sub").on("click", function(e){
        var parentli = $(this).closest('li');
        if(parentli.hasClass('opened')) {
            parentli.removeClass('opened');
            parentli.find('> ul.sub-menu').slideUp(400);
        } else {
            parentli.addClass('opened');
            parentli.find('> ul.sub-menu').slideDown(400);
        }
        parentli.siblings('li').removeClass('opened');
        parentli.siblings('li').find('.has-submenu.opened').removeClass('opened');
        parentli.siblings('li').find('ul:visible').slideUp();
    })
    $('.mobile-menu-btn').on("click", function(){
        $('.overlay-menu').toggleClass("active");
        $(".y-mobile-menu").toggleClass("show");
        return false;
    })
    $('.overlay-menu, .m-menu-close').on("click", function(){
        closeMenu();
    })

    if($('.js-home-slider').length >0) {
        $('.js-home-slider').owlCarousel({
            loop: true,
            // autoplay: true,
            // autoplayTimeout: 4000,
            // nestedItemSelector: 'item-gall',
            margin: 0,
            responsiveClass:true,
            items: 1,
            dots: true,
            navText : ["<i class='fas fa-chevron-left'></i>","<i class='fas fa-chevron-right'></i>"],
            rewindNav : true,
            nav: true,
        });
    }
    if($('.js-slider-2').length >0) {
        $('.js-slider-2').owlCarousel({
            loop: false,
            // autoplay: true,
            // autoplayTimeout: 4000,
            // nestedItemSelector: 'item-gall',
            margin: 20,
            responsiveClass:true,
            items: 4,
            dots: true,
            navText : ["<i class='fas fa-chevron-left'></i>","<i class='fas fa-chevron-right'></i>"],
            rewindNav : true,
            nav: false,
            responsive : {
                // breakpoint from 0 up
                0 : {
                   items: 1,
                },
                480 : {
                    items: 2,
                },
                768 : {
                    items: 3,
                },
                1200 : {
                    items: 4,
                }
            }
        });
    }

    if($('.js-slider-prod').length >0) {
        $('.js-slider-prod').owlCarousel({
            loop: false,
            // autoplay: true,
            // autoplayTimeout: 4000,
            // nestedItemSelector: 'item-gall',
            margin: 20,
            responsiveClass:true,
            items: 4,
            dots: false,
            navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
            rewindNav : true,
            nav: true,
            responsive : {
                // breakpoint from 0 up
                0 : {
                   items: 1,
                },
                480 : {
                    items: 2,
                },
                768 : {
                    items: 3,
                },
                1200 : {
                    items: 4,
                }
            }
        });
    }
    // Scroll To Top
    $(".scroll-top-btn-desktop").on("click", function() {
        $('html,body').animate({ scrollTop: 0 }, 'slow');
        return false;
    });
    window.addEventListener('scroll', function() {
        if (window.pageYOffset > 300) {
            $(".scroll-top-btn-desktop").addClass("visible");
        } else {
            $(".scroll-top-btn-desktop").removeClass("visible");
        }
    });

    if($(window).width()<=767) {
        $('.footer-title').click(function() {
            console.log("adsfd");
            $(this).toggleClass('active');
        })
    }

    $(".js-articleMore").on("click", function () {
        $(this).closest(".product-article").addClass("visibled")
    })
});