$(document).ready(function () {
  // $(window).bind("load resize scroll", function () {
  //   if ($(document).scrollTop() > 174) {
  //     $('.box-menu-header').addClass('header-fixed');
  //   } else {
  //     $('.box-menu-header').removeClass('header-fixed');
  //   }
  // });

  // Menu Mobile
  function closeMenu() {
    $('.y-mobile-menu').removeClass('show');
    $('.overlay-menu').removeClass('active');
  }

  $(".has-submenu > .btn-toggle-sub").on("click", function (e) {
    var parentli = $(this).closest('li');
    if (parentli.hasClass('opened')) {
      parentli.removeClass('opened');
      parentli.find('> ul.sub-menu').slideUp(400);
    } else {
      parentli.addClass('opened');
      parentli.find('> ul.sub-menu').slideDown(400);
    }
    parentli.siblings('li').removeClass('opened');
    parentli.siblings('li').find('.has-submenu.opened').removeClass('opened');
    parentli.siblings('li').find('ul:visible').slideUp();
  })
  $('.mobile-menu-btn, .mobile-menu-btn-footer').on("click", function () {
    $('.overlay-menu').toggleClass("active");
    $(".y-mobile-menu").toggleClass("show");
    return false;
  })
  $('.overlay-menu, .m-menu-close').on("click", function () {
    closeMenu();
  })

  // if ($('.js-home-slider').length > 0) {
  //   $('.js-home-slider').owlCarousel({
  //     loop: true,
  //     // autoplay: true,
  //     // autoplayTimeout: 4000,
  //     // nestedItemSelector: 'item-gall',
  //     margin: 0,
  //     responsiveClass: true,
  //     items: 1,
  //     dots: true,
  //     navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
  //     rewindNav: true,
  //     nav: true,
  //   });
  // }
  if ($('.js-slider-2').length > 0) {
    $('.js-slider-2').owlCarousel({
      loop: false,
      // autoplay: true,
      // autoplayTimeout: 4000,
      // nestedItemSelector: 'item-gall',
      margin: 20,
      responsiveClass: true,
      items: 4,
      dots: true,
      navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
      rewindNav: true,
      nav: false,
      responsive: {
        // breakpoint from 0 up
        0: {
          items: 1,
        },
        480: {
          items: 2,
        },
        768: {
          items: 3,
        },
        1200: {
          items: 4,
        }
      }
    });
  }

  if ($('.js-slider-prod').length > 0) {
    $('.js-slider-prod').owlCarousel({
      loop: false,
      margin: 20,
      responsiveClass: true,
      items: 4,
      dots: false,
      navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
      rewindNav: true,
      nav: true,
      responsive: {
        // breakpoint from 0 up
        0: {
          items: 2,
        },
        480: {
          items: 2,
        },
        768: {
          items: 3,
        },
        1200: {
          items: 4,
        }
      }
    });
  }

  if ($('.js-slider-prod-1').length > 0) {
    var temp = $('.is-article').length ? 3 : 4;
    var hasNav = $('.tab-content-toc .product-section-slider').length ? true : false;
    $('.js-slider-prod-1').owlCarousel({
      loop: false,
      margin: 20,
      responsiveClass: true,
      dots: false,
      navText: ["<span></span>", "<span></span>"],
      rewindNav: true,
      nav: hasNav,
      responsive: {
        // breakpoint from 0 up
        0: {
          items: 2,
          nav: false,
        },
        480: {
          items: 2,
          nav: false,
        },
        768: {
          items: temp
        }
      }
    });
  }


  if ($('.js-feedback-slider').length > 0) {
    $('.js-feedback-slider').owlCarousel({
      loop: false,
      margin: 20,
      responsiveClass: true,
      items: 1,
      dots: true,
      navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
      rewindNav: true,
      nav: true,
      responsive: {
        // breakpoint from 0 up
        0: {
          items: 1,
          nav: false,
          dots: true
        },
        480: {
          items: 1,
          nav: false,
          dots: true
        },
        768: {
          items: 1,
          nav: false,
          dots: true
        },
        992: {
          items: 1,
          nav: true,
          dots: false
        },
        1200: {
          items: 3,
        }
      }
    });
  }
  // Scroll To Top
  $(".scroll-top-btn-desktop").on("click", function () {
    $('html,body').animate({
      scrollTop: 0
    }, 'slow');
    return false;
  });
  window.addEventListener('scroll', function () {
    if (window.pageYOffset > 300) {
      $(".scroll-top-btn-desktop").addClass("visible");
    } else {
      $(".scroll-top-btn-desktop").removeClass("visible");
    }
  });

  if ($(window).width() <= 767) {
    $('.footer-title').click(function () {
      $(this).toggleClass('active');
    })
  }

  $(".js-articleMore").on("click", function () {
    $(this).closest(".product-article").addClass("visibled")
  })

  $(".js-scroll-top").on("click", function () {
    $('html,body').animate({
      scrollTop: 0
    }, 'slow');
    return false;
  });


  $("#m-input-autocomplete").keyup(function () {
    $('.m-form-search-autocomplete').addClass('show');
  });
  $('[data-toggle="tooltip"]').tooltip();

  var sync1 = $(".js-home-slider");
  var sync2 = $(".thumbnail-slider");
  var slidesPerPage = 5; //globaly define number of elements per page
  var syncedSecondary = true;

  if (sync1.length) {
    sync1.owlCarousel({
      autoplay: true,
      autoplayTimeout: 5000,
      loop: true,
      margin: 0,
      responsiveClass: true,
      items: 1,
      dots: false,
      navText: ["<i class='icon-left-arrow'></i>", "<i class='icon-left-arrow'></i>"],
      rewindNav: true,
      nav: true,
      responsiveRefreshRate: 200,
      responsive: {
        // breakpoint from 0 up
        0: {
          dots: true,
          nav: false
        },
        768: {
          dots: false,
          nav: true
        }
      }
    }).on('changed.owl.carousel', syncPosition);
  }

  if (sync2.length) {
    sync2.on('initialized.owl.carousel', function () {
      sync2.find(".owl-item").eq(0).addClass("current");
    })
      .owlCarousel({
        items: slidesPerPage,
        dots: false,
        nav: false,
        touchDrag: false,
        mouseDrag: false,
        smartSpeed: 200,
        slideSpeed: 500,
        slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
        responsiveRefreshRate: 100,
        responsive: {
          // breakpoint from 0 up
          0: {
            items: 3,
            nav: false,
            dots: true
          },
          320: {
            items: 3,
          },
          768: {
            items: 3,
          },
          992: {
            items: 4,
          },
          1200: {
            items: 4,
          },
          1400: {
            items: 5,
          }
        }
      }).on('changed.owl.carousel', syncPosition2);
  }

  function syncPosition(el) {
    //if you set loop to false, you have to restore this next line
    //var current = el.item.index;

    //if you disable loop you have to comment this block
    var count = el.item.count - 1;
    var current = Math.round(el.item.index - (el.item.count / 2) - .5);

    if (current < 0) {
      current = count;
    }
    if (current > count) {
      current = 0;
    }

    //end block

    sync2
      .find(".owl-item")
      .removeClass("current")
      .eq(current)
      .addClass("current");
    var onscreen = sync2.find('.owl-item.active').length - 1;
    var start = sync2.find('.owl-item.active').first().index();
    var end = sync2.find('.owl-item.active').last().index();

    if (current > end) {
      sync2.data('owl.carousel').to(current, 100, true);
    }
    if (current < start) {
      sync2.data('owl.carousel').to(current - onscreen, 100, true);
    }
  }

  function syncPosition2(el) {
    if (syncedSecondary) {
      var number = el.item.index;
      if ($(".js-home-slider").length) {
        sync1.data('owl.carousel').to(number, 100, true);
      }
    }
  }


  sync2.on("click", ".owl-item", function (e) {
    e.preventDefault();
    var number = $(this).index();
    sync1.data('owl.carousel').to(number, 300, true);

    sync2.find(".owl-item").removeClass("current").eq(number).addClass("current");
  });

  // Promotion slider
  if ($('.js-promotion-tiny').length > 0) {
    const tnsCarousel = document.querySelectorAll('.js-promotion-tiny');
    tnsCarousel.forEach(slider => {
      const tnsSlider = tns({
        container: slider,
        arrowKeys: true,
        autoplay: false,
        controls: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayButtonOutput: false,
        autoplayHoverPause: true,
        controlsText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        // controlsContainer: ".js-controls",
        gutter: 0,
        items: 6,
        loop: false,
        mouseDrag: true,
        nav: false,
        responsive: {
          320: {
            items: 2,
          },
          768: {
            items: 3
          },
          992: {
            items: 3
          },
          1200: {
            items: 4
          },

        },
        slideBy: "page",
        touch: true
      });
    });
  }

  // Product slider
  if ($('.js-product-tiny').length > 0) {
    const tnsCarousel = document.querySelectorAll('.js-product-tiny');
    tnsCarousel.forEach(slider => {
      const tnsSlider = tns({
        container: slider,
        arrowKeys: true,
        autoplay: false,
        controls: true,
        autoplayButtonOutput: false,
        autoplayHoverPause: true,
        controlsText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        // controlsContainer: ".js-controls",
        gutter: 0,
        items: 6,
        loop: false,
        mouseDrag: true,
        nav: false,
        responsive: {
          320: {
            items: 2,
          },
          768: {
            items: 3
          },
          992: {
            items: 3
          },
        },
        slideBy: "page",
        touch: true
      });
    });
  }

  // Category slider
  if ($('.js-category-tiny').length > 0) {
    var slider = tns({
      arrowKeys: true,
      container: ".js-category-tiny",
      controls: true,
      autoplay: false,
      autoplayTimeout: 5000,
      autoplayButtonOutput: false,
      autoplayHoverPause: true,
      controlsText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
      // controlsContainer: ".js-controls",
      gutter: 0,
      items: 6,
      loop: false,
      mouseDrag: true,
      nav: false,
      responsive: {
        320: {
          items: 3.5,
        },
        768: {
          items: 3.5
        },
        992: {
          items: 6.5
        },
      },
      slideBy: "page",
      touch: true
    });
  }

  // Branch slider
  if ($('.js-branch-tiny').length > 0) {
    var slider = tns({
      arrowKeys: true,
      container: ".js-branch-tiny",
      controls: true,
      autoplay: false,
      autoplayTimeout: 5000,
      autoplayButtonOutput: false,
      autoplayHoverPause: true,
      controlsText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
      // controlsContainer: ".js-controls",
      gutter: 0,
      items: 4,
      loop: false,
      mouseDrag: true,
      nav: false,
      responsive: {
        320: {
          items: 3.5,
        },
        768: {
          items: 3.5
        },
        992: {
          items: 4.5
        },
      },
      slideBy: "page",
      touch: true
    });
  }

  if ($('.js-choose-slider').length > 0) {
    $('.js-choose-slider').owlCarousel({
      loop: false,
      autoplay: true,
      autoplayTimeout: 5000,
      // nestedItemSelector: 'item-gall',
      margin: 0,
      responsiveClass: true,
      items: 5,
      dots: false,
      navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
      rewindNav: true,
      nav: true,
      responsive: {
        // breakpoint from 0 up
        0: {
          items: 4,
        },
        480: {
          items: 4,
        },
        768: {
          items: 4,
        },
        1200: {
          items: 5,
        }
      }
    });
  }

  if ($('.js-newspaper-tiny').length > 0) {
    var slider = tns({
      arrowKeys: true,
      container: ".js-newspaper-tiny",
      controls: true,
      autoplay: false,
      autoplayTimeout: 5000,
      autoplayButtonOutput: false,
      autoplayHoverPause: true,
      controlsText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
      // controlsContainer: ".js-controls",
      gutter: 0,
      items: 5,
      loop: false,
      mouseDrag: true,
      nav: false,
      responsive: {
        320: {
          items: 3.5,
        },
        768: {
          items: 3.5
        },
        992: {
          items: 5.5
        },
      },
      slideBy: "page",
      touch: true
    });
  }

  // Feedback Slider
  if ($('.js-feedback-tiny').length > 0) {
    var slider = tns({
      arrowKeys: true,
      container: ".js-feedback-tiny",
      controls: true,
      autoplay: false,
      autoplayTimeout: 5000,
      autoplayButtonOutput: false,
      autoplayHoverPause: true,
      controlsText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
      // controlsContainer: ".js-controls",
      gutter: 20,
      items: 4,
      loop: false,
      mouseDrag: true,
      nav: false,
      responsive: {
        320: {
          items: 1,
        },
        768: {
          items: 3
        },
        992: {
          items: 4
        },
      },
      slideBy: "page",
      touch: true
    });
  }

});

function formatMoney(num) {
  if (num) num = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
  return num;
}

$(document).ready(function () {
  var progress = null;
  $(".form-search-autocomplete input").keyup(function () {
    if ($(this).val()) {
      var url = '/api/product/fuzzy-search?q=' + $(this).val();
      var e = "/api/suggest-search?type=product&q=" + $(this).val(),
        t = "";
      progress = $.ajax({
        type: 'GET',
        // url: e,
        url: url,
        beforeSend: function () {
          if (progress != null) {
            progress.abort();
          }
        },
        success: function (e) {
          if (e.data.length > 0) {
            $.each(e.data, function (e, i) {
              if (i.price > 0) {
                if (i.image) {
                  t += ` <div class="item-result">
                          <a href="${i.url}" class="img-prod"><img src="/uploads/${i.image}"></a>
                          <div class="info-prod">
                          <a href="${i.url}">
                              <span class="name-prod">${i.title}</span>
                              <span class="price-prod">${formatMoney(i.price)} đ</span>
                          </a>
                          </div>
                      </div>`
                } else {
                  t += ` <div class="item-result">
                          <div class="info-prod">
                          <a href="${i.url}">
                              <span class="name-prod">${i.title}</span>
                              <span class="price-prod">${formatMoney(i.price)} đ</span>
                          </a>
                          </div>
                      </div>`
                }
              } else {
                if (i.image) {
                  t += ` <div class="item-result">
                          <a href="${i.url}" class="img-prod"><img src="/uploads/${i.image}"></a>
                          <div class="info-prod">
                          <a href="${i.url}">
                              <span class="name-prod">${i.title}</span>
                              <span class="price-prod">Liên hệ</span>
                          </a>
                          </div>
                      </div>`
                } else {
                  t += ` <div class="item-result">
                          <div class="info-prod">
                          <a href="${i.url}">
                              <span class="name-prod">${i.title}</span>
                              <span class="price-prod">Liên hệ</span>
                          </a>
                          </div>
                      </div>`
                }
              }
            });
            $(".box-search-autocomplete").html(t);
            $('.form-search-autocomplete').addClass('show');
          } else {
            $('.form-search-autocomplete').removeClass('show');
          }
        },
        complete: function () {
          progress = null;
        }
      });
    } else {
      $('.form-search-autocomplete').removeClass('show');
    }
  })
});
$(document).ready(function () {
  $(".m-form-search input").keyup(function () {
    if ($(this).val()) {
      $('.m-form-search, .m-form-search-autocomplete').addClass('show');

      var e = "/api/product/fuzzy-search?type=product&q=" + $(this).val(),
        t = "";
      $.get(e, function (e) {
        if (e.data.length > 0) {
          $.each(e.data, function (e, i) {
            if (i.price > 0) {
              if (i.image) {
                t += ` <div class="item-result">
                        <a href="${i.url}" class="img-prod"><img src="/uploads/${i.image}" alt="${i.title}"></a>
                        <div class="info-prod">
                        <a href="${i.url}">
                            <span class="name-prod">${i.title}</span>
                            <span class="price-prod">${formatMoney(i.price)} đ</span>
                        </a>
                        </div>
                    </div>`
              } else {
                t += ` <div class="item-result">
                        <div class="info-prod">
                        <a href="${i.url}">
                            <span class="name-prod">${i.title}</span>
                            <span class="price-prod">${formatMoney(i.price)} đ</span>
                        </a>
                        </div>
                    </div>`
              }

            } else {
              if (i.image) {
                t += ` <div class="item-result">
                        <a href="${i.url}" class="img-prod"><img src="/uploads/${i.image}" alt="${i.title}"></a>
                        <div class="info-prod">
                        <a href="${i.url}">
                            <span class="name-prod">${i.title}</span>
                            <span class="price-prod">Liên hệ</span>
                        </a>
                        </div>
                    </div>`

              } else {
                t += ` <div class="item-result">
                        <div class="info-prod">
                        <a href="${i.url}">
                            <span class="name-prod">${i.title}</span>
                            <span class="price-prod">Liên hệ</span>
                        </a>
                        </div>
                    </div>`

              }

            }
          }), $(".m-box-search-autocomplete").html(t)
        } else {
          $('.m-form-search, .m-form-search-autocomplete').removeClass('show');
        }
      })
    } else {
      $('.m-form-search,.m-form-search-autocomplete').removeClass('show');
    }
  })
});


const LEE = (function ($) {

  function createFAQ(obj, callback) {
    $.post('/api/faq', obj, function (res) {
      if (typeof callback === 'function') {
        return callback(res);
      }
      return alert(res.message);
    });
  }

  function likeFAQ(id, type, callback) {
    $.post('/api/faq/like', { id, type }, function (res) {
      if (typeof callback === 'function') {
        return callback(res);
      }
      return alert(res.message);
    })
  }

  function likedFAQ(id, product_id) {
    var key = 'faq_' + product_id + '_' + id;
    var check = localStorage.getItem(key);
    if (check) {
      localStorage.removeItem(key);
    } else {
      localStorage.setItem(key, 1);
    }
  }

  return {
    createFAQ,
    likeFAQ,
    likedFAQ
  };

})(jQuery);

$('.create-question').submit(function (e) {
  e.preventDefault();
  var data = $(this).serialize();
  var self = $(this);

  LEE.createFAQ(data, function (res) {
    if (res.success) {
      toastr.success('Câu hỏi của bạn đã được ghi nhận');
      self.find('.form-control').val('');
    } else {
      toastr.error(res.message);
    }
  });
});

$('.btn-reply-faq').click(function () {
  $(this).parent().find('.reply-question').slideDown(500);
});

$('.reply-question').submit(function (e) {
  e.preventDefault();
  var data = $(this).serialize();
  var self = $(this);

  LEE.createFAQ(data, function (res) {
    if (res.success) {
      toastr.success('Câu trả lời của bạn đã được ghi nhận');
      self.find('.form-control').val('');
    } else {
      toastr.error(res.message);
    }
  });
});

$('.btn-like-faq').click(function () {
  var self = $(this);
  var id = self.data('id');
  var type = self.hasClass('liked') ? 'unlike' : 'like';

  LEE.likeFAQ(id, type, function (res) {
    if (res.success) {
      self.closest('.item-faq').find('.number-like').html(res.like);
      var text = self.find('span');
      LEE.likedFAQ(id, product_id);
      if (type == 'like') {
        text.text('Đã thích');
        self.addClass('liked')
      } else {
        text.text('Thích');
        self.removeClass('liked')
      }
    } else {
      toastr.error(res.message);
    }
  });
});

$(document).ready(function () {
  $('.item-faq').each(function () {
    var id = $(this).data('id');
    var key = 'faq_' + product_id + '_' + id;
    var check = localStorage.getItem(key);
    if (check) {
      $(this).find('.btn-like-faq').addClass('liked');
      $(this).find('.btn-like-faq span').text('Đã thích');
    }
  });
});

$(document).ready(function () {
  var rm = $("#toggle"),
    moreText = "Xem thêm",
    lessText = "Rút gọn";

  rm.click(function () {
    var $this = $(this);
    $this.prev().slideToggle();
    $this.text($this.text() == moreText ? lessText : moreText);
  });
})
// $('.drop-down').hover(function(){
//   $(this).find('.sub-menu').toggle();
// })

$(document).on('click', '.btn-buy-product', function () {
  var variant_id = $(this).data('variant_id');
  StoreAPI.addItem(variant_id, 1, function () {
    toastr.success("Thêm vào giỏ hàng thành công");
    setTimeout(function () {
      location.href = "/dat-hang";
    }, 1000);
  });
});